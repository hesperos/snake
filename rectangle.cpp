#include "rectangle.h"

namespace snake
{
    Rectangle::Rectangle(Point topLeft, Point bottomRight) :
        topLeft(topLeft),
        bottomRight(bottomRight)
    {
    }

    Point Rectangle::getTopLeft() const noexcept
    {
        return topLeft;
    }

    Point Rectangle::getBottomRight() const noexcept
    {
        return bottomRight;
    }

    std::size_t Rectangle::getWidth() const noexcept
    {
        return bottomRight.first - topLeft.first;
    }

    std::size_t Rectangle::getHeight() const noexcept
    {
        return bottomRight.second - topLeft.second;
    }

} /* namespace snake */
