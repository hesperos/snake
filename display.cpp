#include "display.h"

namespace snake
{
    Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) :
        r(r),
        g(g),
        b(b),
        a(a)
    {
    }

    Display::Display(Point dimensions) :
        dimensions(std::move(dimensions))
    {
    }

    Point Display::getDimensions() const noexcept
    {
        return dimensions;
    }
} /* namespace snake */
