#include "object_factory.h"

namespace snake
{
    ObjectFactory::ObjectFactory(std::size_t spriteWidth,
            std::size_t spriteHeight,
            std::size_t mapWidth,
            std::size_t mapHeight,
            std::shared_ptr<snake::Texture> texture,
            std::shared_ptr<snake::Texture> background) :
        spriteWidth(spriteWidth),
        spriteHeight(spriteHeight),
        mapWidth(mapWidth),
        mapHeight(mapHeight),
        texture(texture),
        background(background)
    {
    }

    std::unique_ptr<snake::Snake> ObjectFactory::createSnake(
            Point point, Direction direction)
    {
        std::unique_ptr<snake::SpriteSheet> snakeSs =
            std::make_unique<snake::SpriteSheet>(texture,
                    spriteWidth,
                    spriteHeight);

        return std::make_unique<snake::Snake>(point,
                direction,
                std::move(snakeSs));
    }

    std::unique_ptr<snake::Food> ObjectFactory::createFood(
            Point point)
    {
        std::unique_ptr<snake::SpriteSheet> appleSs =
            std::make_unique<snake::SpriteSheet>(texture,
                    spriteWidth,
                    spriteHeight);

        return std::make_unique<snake::Food>(std::move(point),
               std::move(appleSs));
    }

    std::unique_ptr<snake::Map> ObjectFactory::createMap(
            std::filesystem::path p)
    {
        std::unique_ptr<snake::SpriteSheet> mapSs =
            std::make_unique<snake::SpriteSheet>(background,
                    spriteWidth,
                    spriteHeight);

        return std::make_unique<snake::Map>(p,
                std::move(mapSs));
    }
} /* namespace snake */
