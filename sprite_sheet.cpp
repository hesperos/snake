#include "sprite_sheet.h"

#include <stdexcept>

namespace snake
{
    SpriteSheet::SpriteSheet(std::shared_ptr<Texture> texture,
            size_t spriteWidth,
            size_t spriteHeight) :
        texture(std::move(texture)),
        spriteWidth(spriteWidth),
        spriteHeight(spriteHeight),
        stride(this->texture->getWidth()/spriteWidth),
        nSprites(stride * (this->texture->getHeight()/spriteHeight)),
        sprites(mapSprites())
    {
    }

    std::vector<Sprite> SpriteSheet::mapSprites()
    {
        std::vector<Sprite> sprites;
        for (size_t n = 0; n <= nSprites; ++n)
        {
            Point topLeft(spriteWidth * (n % stride),
                    spriteHeight * (n / stride));

            Point bottomRight(topLeft.first + spriteWidth,
                    topLeft.second + spriteHeight);

            sprites.emplace_back(Rectangle{topLeft, bottomRight});
        }

        return sprites;
    }

    size_t SpriteSheet::size() const noexcept
    {
        return nSprites;
    }

    Texture& SpriteSheet::getTexture() const noexcept
    {
        return *texture;
    }

    const Sprite& SpriteSheet::at(size_t n) const
    {
        return sprites.at(n);
    }

} /* namespace snake */
