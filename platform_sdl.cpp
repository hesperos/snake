#include "platform_sdl.h"

#include <SDL.h>
#include <SDL_image.h>

#include <memory>

namespace
{
    const char* const windowTitle = "snake";
    int tileSize = 0;
}

namespace
{
    inline SDL_Rect toSdlRect(snake::Rectangle r)
    {
        const auto& topLeft = r.getTopLeft();
        return SDL_Rect{topLeft.first,
            topLeft.second,
            static_cast<int>(r.getWidth()),
            static_cast<int>(r.getHeight())};
    }

    class SdlSurface
    {
    public:
        explicit SdlSurface(SDL_Surface* s) :
            surface(s, &SDL_FreeSurface)
        {
            if (surface == nullptr)
            {
                throw std::runtime_error("surface must be != nullptr");
            }
        }

        SDL_Surface* getSurface() const noexcept
        {
            return surface.get();
        }

    private:
        std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)> surface;
    };

    class SdlTexture :
        public snake::Texture
    {
    public:
        explicit SdlTexture(SDL_Texture* t) :
            texture(t, &SDL_DestroyTexture),
            width(0),
            height(0)
        {
            if (texture == nullptr)
            {
                throw std::runtime_error("texture must be != nullptr");
            }

            if (SDL_QueryTexture(texture.get(), NULL,
                    NULL,
                    &width,
                    &height))
            {
                throw std::runtime_error(
                        std::string("unable to query texture details: ") +
                        SDL_GetError());
            }
        }

        SDL_Texture* getTexture() const
        {
            return texture.get();
        }

        std::size_t getWidth() const override
        {
            return width;
        }

        std::size_t getHeight() const override
        {
            return height;
        }

    private:
        std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)> texture;
        int width;
        int height;
    };

    class SdlDisplay :
        public snake::Display
    {
    public:
        SdlDisplay(size_t w, size_t h) :
            snake::Display(snake::Point(w, h)),
            window(SDL_CreateWindow(windowTitle,
                        SDL_WINDOWPOS_UNDEFINED,
                        SDL_WINDOWPOS_UNDEFINED,
                        w * tileSize,
                        h * tileSize,
                        SDL_WINDOW_SHOWN),
                    &SDL_DestroyWindow),
            renderer(SDL_CreateRenderer(window.get(),
                        -1,
                        SDL_RENDERER_ACCELERATED),
                    &SDL_DestroyRenderer)
        {
        }

        ~SdlDisplay()
        {
        }

        std::unique_ptr<snake::Texture>
            createTexture(std::filesystem::path p) override
        {
            SDL_Surface* sdlSurface = IMG_Load(p.c_str());
            if (sdlSurface == nullptr)
            {
                throw std::runtime_error(
                        std::string("unable to load texture: ") +
                        IMG_GetError());
            }

            SdlSurface surface{sdlSurface};
            return std::make_unique<SdlTexture>(
                    SDL_CreateTextureFromSurface(renderer.get(),
                        surface.getSurface()));
        }

        void renderTile(snake::Point point, snake::Color color) override
        {
            SDL_Rect rect{
                tileSize * point.first,
                tileSize * point.second,
                tileSize,
                tileSize,
            };

            SDL_SetRenderDrawColor(renderer.get(),
                    color.r,
                    color.g,
                    color.b,
                    color.a);
            SDL_RenderFillRect(renderer.get(), &rect);
        }

        void renderTile(snake::Point point,
                const snake::SpriteSheet& ss,
                size_t n) override
        {
            SDL_Rect srcRect = toSdlRect(ss.at(n).getBoundingBox());
            SDL_Rect dstRect{
                tileSize * point.first,
                tileSize * point.second,
                tileSize,
                tileSize,
            };

            SDL_RenderCopy(renderer.get(),
                    dynamic_cast<SdlTexture&>(ss.getTexture()).getTexture(),
                    &srcRect,
                    &dstRect);
        }

        void clear() override
        {
            SDL_SetRenderDrawColor(renderer.get(),
                    0x00, 0x00, 0x00, 0xff);
            SDL_RenderClear(renderer.get());
        }

        void flush() override
        {
            SDL_RenderPresent(renderer.get());
        }

    private:
        std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> window;
        std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> renderer;
    };

    class SdlInput :
        public snake::Input
    {
    public:
        Symbol getSymbol() const override
        {
            switch(lastEvent.type)
            {
                case SDL_QUIT:
                    return Input::Symbol::Esc;
                    break;

                case SDL_KEYDOWN:
                    switch(lastEvent.key.keysym.sym)
                    {
                        case SDLK_q:
                            return Input::Symbol::Esc;
                            break;

                        case SDLK_LEFT:
                            return Input::Symbol::Left;
                            break;

                        case SDLK_RIGHT:
                            return Input::Symbol::Right;
                            break;

                        case SDLK_UP:
                            return Input::Symbol::Up;
                            break;

                        case SDLK_DOWN:
                            return Input::Symbol::Down;
                            break;
                    }
                    break;
            }

            return Input::Symbol::None;
        }

        std::size_t getSymbols() const override
        {
            const int timeoutMs = 5;
            if (!SDL_WaitEventTimeout(&lastEvent, timeoutMs))
            {
                return 0;
            }

            return 1;
        }

    private:
        mutable SDL_Event lastEvent;
    };
} /* namespace  */

namespace snake
{
    PlatformSdl::PlatformSdl(size_t tileWidth, size_t tileHeight)
    {
        tileSize = tileWidth;
        if (tileWidth != tileHeight)
        {
            throw std::runtime_error("tiles must be square shaped");
        }

        SDL_Init(SDL_INIT_VIDEO);

        int imgFlags = IMG_INIT_PNG;
        if (!(IMG_Init(imgFlags) & imgFlags))
        {
            throw "unable to initialise png texture loader";
        }
    }

    PlatformSdl::~PlatformSdl()
    {
        IMG_Quit();
        SDL_Quit();
    }

    std::unique_ptr<snake::Display>
    PlatformSdl::createDisplay(size_t w, size_t h)
    {
        return std::make_unique<SdlDisplay>(w, h);
    }

    std::unique_ptr<snake::Input>
    PlatformSdl::createInput()
    {
        return std::make_unique<SdlInput>();
    }

    std::chrono::milliseconds PlatformSdl::getMilli() const
    {
        return std::chrono::milliseconds(SDL_GetTicks());
    }
} /* namespace snake */
