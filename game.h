#ifndef GAME_H_
#define GAME_H_

#include "display.h"
#include "food.h"
#include "input.h"
#include "object_factory.h"
#include "snake.h"

#include <atomic>
#include <deque>
#include <memory>
#include <random>

namespace snake
{
    class Game
    {
    public:
        Game(int argc,
                const char** argv,
                std::unique_ptr<snake::ObjectFactory> objectFactory,
                std::unique_ptr<snake::Display> display,
                std::unique_ptr<snake::Input> input);

        bool isRunning() const noexcept;
        void processInput();
        size_t getSnakeLength() const;

        bool consumeFood();
        std::unique_ptr<snake::Food> generateFood();
        std::unique_ptr<snake::Snake> createSnake();

        void update();
        void render();

    protected:
        std::unique_ptr<snake::Display> display;
        std::unique_ptr<snake::Input> input;

        std::unique_ptr<snake::ObjectFactory> objectFactory;
        std::unique_ptr<snake::Snake> snake;
        std::unique_ptr<snake::Food> food;
        Direction direction;

        std::atomic<bool> isRunning_;
        std::atomic<bool> isOver;

    private:
        std::random_device rd;
        std::mt19937 rng;
    };

} /* namespace snake */

#endif /* GAME_H_ */
