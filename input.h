#ifndef INPUT_H_
#define INPUT_H_

#include <utility>

namespace snake
{
    class Input
    {
    public:
        enum class Symbol
        {
            Up,
            Down,
            Left,
            Right,

            Esc,
            None,
        };

        virtual ~Input() = default;

        virtual Symbol getSymbol() const = 0;
        virtual std::size_t getSymbols() const = 0;
    };

} /* namespace snake */

#endif /* INPUT_H_ */
