#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "point.h"

namespace snake
{
    class Rectangle
    {
    public:
        Rectangle(Point topLeft, Point bottomRight);

        Point getTopLeft() const noexcept;
        Point getBottomRight() const noexcept;
        std::size_t getWidth() const noexcept;
        std::size_t getHeight() const noexcept;

    private:
        Point topLeft;
        Point bottomRight;
    };
} /* namespace snake */

#endif /* RECTANGLE_H_ */
