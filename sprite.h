#ifndef SPRITE_H_
#define SPRITE_H_

#include "rectangle.h"

#include <memory>

namespace snake
{
    /**
     * @brief Represents a single frame out of spritesheet
     */
    class Sprite
    {
    public:
        Sprite(Rectangle rectangle);

        Rectangle getBoundingBox() const;

    private:
        Rectangle rectangle;
    };
} /* namespace snake */

#endif /* SPRITE_H_ */
