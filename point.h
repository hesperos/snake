#ifndef POINT_H_
#define POINT_H_

#include <cstdint>
#include <utility>

namespace snake
{
    template <typename CoordinateT>
        using PointType = std::pair<CoordinateT, CoordinateT>;

    using Point = PointType<int32_t>;

} /* namespace snake */

#endif /* POINT_H_ */
