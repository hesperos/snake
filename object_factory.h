#ifndef OBJECT_FACTORY_H_
#define OBJECT_FACTORY_H_

#include "food.h"
#include "map.h"
#include "snake.h"

#include <memory>

namespace snake
{
    class ObjectFactory
    {
    public:
        ObjectFactory(std::size_t spriteWidth,
                std::size_t spriteHeight,
                std::size_t mapWidth,
                std::size_t mapHeight,
                std::shared_ptr<snake::Texture> texture,
                std::shared_ptr<snake::Texture> background);

        std::unique_ptr<snake::Snake> createSnake(Point point,
                Direction direction = Direction::Left);

        std::unique_ptr<snake::Food> createFood(Point point);

        std::unique_ptr<snake::Map> createMap(
                std::filesystem::path p);

    private:
        std::size_t spriteWidth;
        std::size_t spriteHeight;
        std::size_t mapWidth;
        std::size_t mapHeight;

        std::shared_ptr<snake::Texture> texture;
        std::shared_ptr<snake::Texture> background;
    };
} /* namespace snake */

#endif /* OBJECT_FACTORY_H_ */
