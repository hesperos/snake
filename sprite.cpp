#include "sprite.h"

namespace snake
{
    Sprite::Sprite(Rectangle rectangle) :
        rectangle(std::move(rectangle))
    {
    }

    Rectangle Sprite::getBoundingBox() const
    {
        return rectangle;
    }
} /* namespace snake */
