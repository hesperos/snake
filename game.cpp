#include "game.h"
#include "point.h"
#include "snake_ss.h"

namespace snake
{
    spritesheet::Sprites toSprite(Snake::Segment::Type segmentType,
            Direction direction)
    {
        spritesheet::Sprites sprite = spritesheet::Sprites::None0;
        switch(segmentType)
        {
            case Snake::Segment::Type::Head:
                switch(direction)
                {
                    case Direction::Up:
                        sprite = spritesheet::Sprites::HeadUp;
                        break;

                    case Direction::Down:
                        sprite = spritesheet::Sprites::HeadDown;
                        break;

                    case Direction::Left:
                        sprite = spritesheet::Sprites::HeadLeft;
                        break;

                    case Direction::Right:
                        sprite = spritesheet::Sprites::HeadRight;
                        break;
                }
                break;

            case Snake::Segment::Type::Body:
                switch(direction)
                {
                    case Direction::Up:
                    case Direction::Down:
                        sprite = spritesheet::Sprites::BodyHorizontal;
                        break;

                    case Direction::Left:
                    case Direction::Right:
                        sprite = spritesheet::Sprites::BodyVertical;
                        break;
                }
                break;

            case Snake::Segment::Type::Curve:
                switch(direction)
                {
                    case Direction::Up:
                        sprite = spritesheet::Sprites::BodyCurveBottomLeft;
                        break;

                    case Direction::Down:
                        sprite = spritesheet::Sprites::BodyCurveBottomRight;
                        break;

                    case Direction::Left:
                        sprite = spritesheet::Sprites::BodyCurveTopRight;
                        break;

                    case Direction::Right:
                        sprite = spritesheet::Sprites::BodyCurveTopLeft;
                        break;
                }
                break;


            case Snake::Segment::Type::Tail:
                switch(direction)
                {
                    case Direction::Up:
                        sprite = spritesheet::Sprites::TailUp;
                        break;

                    case Direction::Down:
                        sprite = spritesheet::Sprites::TailDown;
                        break;

                    case Direction::Left:
                        sprite = spritesheet::Sprites::TailLeft;
                        break;

                    case Direction::Right:
                        sprite = spritesheet::Sprites::TailRight;
                        break;
                }
                break;

            default:
                break;
        }

        return sprite;
    }

    Game::Game(int,
            const char**,
            std::unique_ptr<snake::ObjectFactory> objectFactory,
            std::unique_ptr<Display> display,
            std::unique_ptr<Input> input) :
        display(std::move(display)),
        input(std::move(input)),
        objectFactory(std::move(objectFactory)),
        snake(createSnake()),
        food(generateFood()),
        direction(Direction::Right),
        isRunning_(true),
        isOver(false),
        rng{rd()}
    {
    }

    bool Game::isRunning() const noexcept
    {
        return isRunning_ && !isOver;
    }

    void Game::processInput()
    {
        if (0 == input->getSymbols())
        {
            return;
        }

        switch(input->getSymbol())
        {
            case Input::Symbol::Up:
                direction = Direction::Up;
                break;
            case Input::Symbol::Down:
                direction = Direction::Down;
                break;
            case Input::Symbol::Left:
                direction = Direction::Left;
                break;
            case Input::Symbol::Right:
                direction = Direction::Right;
                break;

            case Input::Symbol::Esc:
                isRunning_.store(false);
                break;

            default:
                break;
        }
    }

    bool Game::consumeFood()
    {
        if (snake->getHead().getCoord() == food->getPosition())
        {
            return true;
        }

        return false;
    }

    std::unique_ptr<snake::Snake> Game::createSnake()
    {
        const auto& dims = display->getDimensions();
        return objectFactory->createSnake(
                snake::Point(dims.first/2, dims.second/2));
    }

    std::unique_ptr<snake::Food> Game::generateFood()
    {
        const auto& dims = display->getDimensions();
        std::uniform_int_distribution<> xDist(0, dims.first - 1);
        std::uniform_int_distribution<> yDist(0, dims.second - 1);
        snake::Point point;

        do
        {
            point.first = xDist(rng);
            point.second = yDist(rng);
        } while(snake->contains(point));

        return objectFactory->createFood(point);
    }

    void Game::update()
    {
        if (snake->isOroborous())
        {
            isOver.store(true);
        }

        if (consumeFood())
        {
            food = generateFood();
            snake->grow();
        }

        const auto& dims = display->getDimensions();
        snake->move(direction, dims);
    }

    void Game::render()
    {
        display->clear();
        display->renderTile(food->getPosition(),
                food->getSpriteSheet(),
                static_cast<size_t>(spritesheet::Sprites::Apple));

        for (const auto& bodySegment : snake->getBody())
        {
            display->renderTile(bodySegment.getCoord(),
                    snake->getSpriteSheet(),
                    static_cast<size_t>(toSprite(
                            bodySegment.getType(),
                            bodySegment.getOrientation())));
        }

        display->flush();
    }

    size_t Game::getSnakeLength() const
    {
        return snake->size();
    }
} /* namespace snake */
