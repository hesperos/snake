#include "food.h"

namespace snake
{
    Food::Food(snake::Point position,
            std::unique_ptr<snake::SpriteSheet> ss) :
        position(std::move(position)),
        ss(std::move(ss))
    {
    }

    const snake::Point& Food::getPosition() const noexcept
    {
        return position;
    }

    snake::SpriteSheet& Food::getSpriteSheet() const
    {
        return *ss;
    }
} /* namespace snake */
