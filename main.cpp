#include "config.h"
#include "display.h"
#include "game.h"
#include "input.h"
#include "sprite_sheet.h"

#include "platform_sdl.h"

#include <chrono>
#include <iostream>
#include <memory>
#include <thread>

namespace
{
    const size_t spriteWidth = 64;
    const size_t spriteHeight = 64;
} /* namespace  */


int main(int argc, const char *argv[])
{
    const size_t width = 18;
    const size_t height = 10;

    try
    {
        std::unique_ptr<snake::Platform> platform =
            std::make_unique<snake::PlatformSdl>(spriteWidth, spriteHeight);

        std::unique_ptr<snake::Display> display =
            platform->createDisplay(width, height);

        std::unique_ptr<snake::Input> input =
            platform->createInput();

        std::shared_ptr<snake::Texture> texture =
            display->createTexture(
                    snake::config::installPrefixPath /
                    "share/snake/snake_ss.png");

        std::shared_ptr<snake::Texture> background =
            display->createTexture(
                    snake::config::installPrefixPath /
                    "share/snake/groundtiles.png");

        std::unique_ptr<snake::ObjectFactory> objectFactory =
            std::make_unique<snake::ObjectFactory>(spriteWidth, spriteHeight,
                    width, height,
                    texture,
                    background);

        std::unique_ptr<snake::Game> game =
            std::make_unique<snake::Game>(argc,
                    argv,
                    std::move(objectFactory),
                    std::move(display),
                    std::move(input));

        const int fps = 7;
        const auto desiredFrameDuration =
            std::chrono::milliseconds(1000) / fps;

        while (game->isRunning())
        {
            auto frameStart = platform->getMilli();
            game->processInput();
            game->update();
            game->render();
            auto frameDuration = platform->getMilli() - frameStart;

            if (desiredFrameDuration > frameDuration)
            {
                std::this_thread::sleep_for(desiredFrameDuration
                        - frameDuration);
            }
        }

    }
    catch(const std::exception& e)
    {
        std::cerr << "exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "unknown exception, terminating ..." << std::endl;
    }

    return 0;
}
