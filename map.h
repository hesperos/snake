#ifndef MAP_H_
#define MAP_H_

#include "point.h"
#include "sprite_sheet.h"

#include <filesystem>
#include <map>
#include <memory>
#include <vector>

namespace snake
{
    class Map
    {
    public:
        class Tile
        {
        public:
            Tile(size_t spriteIdx, bool isSolid);

        private:
            size_t spriteIdx;
            bool isSolid_;

            bool isSolid() const noexcept;
        };

        Map(std::filesystem::path m,
                std::unique_ptr<SpriteSheet> ss);

        SpriteSheet& getSpriteSheet() const;

    private:
        using Layer = std::map<Point, Tile>;

        std::vector<Layer> gameMap;
        std::unique_ptr<SpriteSheet> ss;

        std::vector<Layer> loadMap(std::filesystem::path m);
    };
} /* namespace snake */


#endif /* MAP_H_ */
