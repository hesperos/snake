#ifndef DIRECTION_H_
#define DIRECTION_H_

namespace snake
{
    enum class Direction
    {
        Up,
        Down,
        Left,
        Right,

        None
    };

    Direction invertDirection(Direction direction);
} /* namespace snake */

#endif /* DIRECTION_H_ */
