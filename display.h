#ifndef DISPLAY_H_
#define DISPLAY_H_


#include "point.h"
#include "sprite_sheet.h"
#include "texture.h"

#include <cstdint>
#include <filesystem>
#include <memory>

namespace snake
{
    class Color
    {
    public:
        Color(uint8_t r = 0,
                uint8_t g = 0,
                uint8_t b = 0,
                uint8_t a = 0);

        uint32_t r : 8;
        uint32_t g : 8;
        uint32_t b : 8;
        uint32_t a : 8;
    };

    class Display
    {
    public:
        virtual ~Display() = default;
        Display(Point dimensions);

        virtual Point getDimensions() const noexcept;
        virtual void renderTile(Point point, Color color) = 0;
        virtual void renderTile(Point point,
                const SpriteSheet& ss,
                size_t n) = 0;

        virtual void clear() = 0;
        virtual void flush() = 0;

        virtual std::unique_ptr<Texture>
            createTexture(std::filesystem::path p) = 0;

    protected:
        Point dimensions;
    };

} /* namespace snake */

#endif /* DISPLAY_H_ */
