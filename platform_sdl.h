#ifndef PLATFORM_SDL_H_
#define PLATFORM_SDL_H_

#include "platform.h"

namespace snake
{
    class PlatformSdl :
        public Platform
    {
    public:
        PlatformSdl(size_t tileWidth, size_t tileHeight);
        ~PlatformSdl();

        std::unique_ptr<snake::Display>
            createDisplay(size_t w, size_t h) override;

        std::unique_ptr<snake::Input>
            createInput() override;

        std::chrono::milliseconds getMilli() const override;
    };
} /* namespace snake */


#endif /* PLATFORM_SDL_H_ */
