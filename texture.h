#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <utility>

namespace snake
{
    class Texture
    {
    public:
        virtual ~Texture() = default;

        virtual std::size_t getWidth() const = 0;
        virtual std::size_t getHeight() const = 0;
    };

} /* namespace snake */
#endif /* TEXTURE_H_ */
