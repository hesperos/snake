#ifndef SNAKE_SS_H_
#define SNAKE_SS_H_

namespace snake
{
    namespace spritesheet
    {
        enum class Sprites
        {
            BodyCurveTopLeft = 0,
            BodyVertical,
            BodyCurveTopRight,
            HeadUp,
            HeadRight,
            BodyCurveBottomLeft,
            None,
            BodyHorizontal,
            HeadLeft,
            HeadDown,
            None0,
            None1,
            BodyCurveBottomRight,
            TailUp,
            TailRight,
            Apple,
            None2,
            None3,
            TailLeft,
            TailDown,
        };

        enum class Background
        {
            Sand1,
            Sand2,
            Grass,
            Wall,
            None0,
            None1,
            Brick,
            None2,
            None3,
        };
    } /* namespace spritesheet */
} /* namespace snake */

#endif /* SNAKE_SS_H_ */
