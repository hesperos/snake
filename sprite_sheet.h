#ifndef SPRITE_SHEET_H_
#define SPRITE_SHEET_H_

#include "sprite.h"
#include "texture.h"

#include <memory>
#include <vector>

namespace snake
{
    class SpriteSheet
    {
    public:
        explicit SpriteSheet(std::shared_ptr<Texture> texture,
                size_t spriteWidth,
                size_t spriteHeight);

        size_t size() const noexcept;
        Texture& getTexture() const noexcept;

        const Sprite& at(size_t n) const;

    private:
        std::shared_ptr<Texture> texture;
        const size_t spriteWidth;
        const size_t spriteHeight;
        const size_t stride;
        const size_t nSprites;
        std::vector<Sprite> sprites;

        std::vector<Sprite> mapSprites();
    };
} /* namespace snake */

#endif /* SPRITE_SHEET_H_ */
