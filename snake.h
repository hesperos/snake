#ifndef SNAKE_H_
#define SNAKE_H_

#include "direction.h"
#include "point.h"
#include "sprite_sheet.h"

#include <cstdint>
#include <deque>
#include <utility>

namespace snake
{
    class Snake
    {
    public:
        class Segment
        {
        public:
            enum class Type
            {
                Head,
                Body,
                Curve,
                Tail,
            };

            Segment(Point point,
                    Direction direction,
                    Type type);

            const Point& getCoord() const noexcept;
            Type getType() const noexcept;
            Direction getOrientation() const noexcept;

            void setType(Type type) noexcept;
            void setCoord(Point coord) noexcept;
            void setOrientation(Direction d) noexcept;

        private:
            Point coord;
            Direction orientation;
            Type type;
        };

        Snake(Point initial,
                Direction initialDirection,
                std::unique_ptr<snake::SpriteSheet> ss);

        void move(Direction direction,
                const Point& boundaries);
        std::size_t size() const noexcept;
        void grow();

        Segment getHead() const noexcept;
        const std::deque<Snake::Segment>& getBody() const noexcept;
        SpriteSheet& getSpriteSheet() const;

        bool contains(const Point& point) const noexcept;
        bool isOroborous() const noexcept;

    protected:
        std::deque<Segment> body;
        std::unique_ptr<SpriteSheet> ss;

        void propagateDirection();
        void fixTailOrientation();
        void fixBodySegments();
    };
} /* namespace snake */

#endif /* SNAKE_H_ */
