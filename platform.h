#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "display.h"
#include "input.h"

#include <chrono>
#include <memory>

namespace snake
{
    class Platform
    {
    public:
        virtual ~Platform() = default;

        virtual std::unique_ptr<snake::Display>
            createDisplay(size_t w, size_t h) = 0;

        virtual std::unique_ptr<snake::Input>
            createInput() = 0;

        virtual std::chrono::milliseconds getMilli() const = 0;
    };
} /* namespace snake */

#endif /* PLATFORM_H_ */
