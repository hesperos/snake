#include "map.h"

namespace snake
{

    Map::Tile::Tile(size_t spriteIdx, bool isSolid) :
        spriteIdx(spriteIdx),
        isSolid_(isSolid)
    {
    }

    bool Map::Tile::isSolid() const noexcept
    {
        return isSolid_;
    }

    Map::Map(std::filesystem::path m,
            std::unique_ptr<snake::SpriteSheet> ss) :
        gameMap(loadMap(m)),
        ss(std::move(ss))
    {
    }

    std::vector<Map::Layer> Map::loadMap(std::filesystem::path m)
    {
        std::vector<Map::Layer> map;

        if (!std::filesystem::exists(m) ||
                std::filesystem::is_directory(m))
        {
            throw std::runtime_error(
                    std::string("requested file does not exist: ") +
                    m.native());
        }

        return std::move(map);
    }

} /* namespace snake */
