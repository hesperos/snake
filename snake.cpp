#include "snake.h"

#include <algorithm>

namespace
{
    template <typename TypeT>
        TypeT signedModulo(TypeT x, TypeT n)
        {
            return (x % n + n) % n;
        }
} /* namespace  */

namespace snake
{
    Snake::Segment::Segment(Point point,
            Direction direction,
            Type type) :
        coord(point),
        orientation(direction),
        type(type)
    {
    }

    const Point& Snake::Segment::getCoord() const noexcept
    {
        return coord;
    }

    Snake::Segment::Type Snake::Segment::getType() const noexcept
    {
        return type;
    }

    Direction Snake::Segment::getOrientation() const noexcept
    {
        return orientation;
    }

    void Snake::Segment::setCoord(Point point) noexcept
    {
        this->coord = point;
    }

    void Snake::Segment::setType(Type type) noexcept
    {
        this->type = type;
    }

    void Snake::Segment::setOrientation(Direction d) noexcept
    {
        orientation = d;
    }

    Snake::Snake(Point initial,
            Direction initialDirection,
            std::unique_ptr<snake::SpriteSheet> ss) :
        ss(std::move(ss))
    {
        body.emplace_back(initial, initialDirection, Segment::Type::Head);
    }

    void Snake::move(Direction direction,
            const Point& boundaries)
    {
        Point h = getHead().getCoord();

        switch(direction)
        {
            case Direction::Up:
                h.second = signedModulo(
                        h.second - 1, boundaries.second);
                break;
            case Direction::Down:
                h.second = signedModulo(
                        h.second + 1, boundaries.second);
                break;
            case Direction::Left:
                h.first = signedModulo(
                        h.first - 1, boundaries.first);
                break;
            case Direction::Right:
                h.first = signedModulo(
                        h.first + 1, boundaries.first);
                break;

            default:
                break;
        }

        body.emplace_front(h, direction, Snake::Segment::Type::Head);
        body.pop_back();
        propagateDirection();
    }

    void Snake::grow()
    {
        body.push_back(body.back());
    }

    std::size_t Snake::size() const noexcept
    {
        return body.size();
    }

    Snake::Segment Snake::getHead() const noexcept
    {
        return body.front();
    }

    const std::deque<Snake::Segment>& Snake::getBody() const noexcept
    {
        return body;
    }

    snake::SpriteSheet& Snake::getSpriteSheet() const
    {
        return *ss;
    }

    bool Snake::contains(const Point& point) const noexcept
    {
        return std::find_if(body.begin(),
                body.end(),
                [&](const auto& segment){
                    return segment.getCoord() == point;
                }) != body.end();
    }


    bool Snake::isOroborous() const noexcept
    {
        return std::find_if(body.begin() + 1,
                body.end(),
                [&](const auto& segment){
                    return segment.getCoord() == body.front().getCoord();
                }) != body.end();
    }

    void Snake::fixTailOrientation()
    {
        // take care of the tail first
        auto previous = std::prev(body.end(), 2);
        auto& tail = body.back();
        tail.setType(Snake::Segment::Type::Tail);

        if (auto tc = tail.getCoord(),
                pc = previous->getCoord(); tc.first == pc.first)
        {
            // vertical
            tail.setOrientation(
                    pc.second > tc.second ?
                    Direction::Down : Direction::Up);
        }
        else
        {
            // horizontal
            tail.setOrientation(
                    pc.first < tc.first ?
                    Direction::Left : Direction::Right);
        }
    }

    void Snake::fixBodySegments()
    {
        /*
         * 12 basic cases
         *
         * 0  1  2  3  4   5   6 7
         *
         *                     *
         *        * *          *
         * x* *x x* *x x** **x x x
         *  * *                  *
         *                       *
         *
         * 8  9  10 11
         *
         *        x x
         * ** ** ** **
         *  x x
         *
         */
        for (auto it = std::next(body.begin(), 1);
                it != std::prev(body.end(), 1);
                ++it)
        {
            auto pt = std::prev(it, 1);
            auto nt = std::next(it, 1);
            const auto& pc = pt->getCoord();
            const auto& cc = it->getCoord();
            const auto& nc = nt->getCoord();

            if (pc.first == cc.first &&
                    pc.first == nc.first)
            {
                // 6 or 7
                it->setType(Segment::Type::Body);
            }
            else if (pc.second == cc.second &&
                    pc.second == nc.second)
            {
                // 4 or 5
                it->setType(Segment::Type::Body);
            }
            else if (pc.first < cc.first &&
                    pc.second == cc.second &&
                    pc.second != nc.second)
            {
                // 0 or 2
                it->setType(Segment::Type::Curve);
                it->setOrientation(pc.second > nc.second ?
                        Direction::Down : Direction::Left);
            }
            else if (pc.first > cc.first &&
                    pc.second == cc.second &&
                    pc.second != nc.second)
            {
                // 1 or 3
                it->setType(Segment::Type::Curve);
                it->setOrientation(pc.second > nc.second ?
                        Direction::Up : Direction::Right);
            }
            else if (pc.first == cc.first &&
                    pc.second > cc.second &&
                    pc.first != nc.first)
            {
                // 8 or 9
                it->setType(Segment::Type::Curve);
                it->setOrientation(pc.first > nc.first ?
                        Direction::Left : Direction::Right);
            }
            else if (pc.first == cc.first &&
                    pc.second < cc.second &&
                    pc.first != nc.first)
            {
                // 10 or 11
                it->setType(Segment::Type::Curve);
                it->setOrientation(pc.first > nc.first ?
                        Direction::Down : Direction::Up);
            }
        }
    }

    void Snake::propagateDirection()
    {
        if (size() <= 1)
        {
            return;
        }

        fixTailOrientation();
        fixBodySegments();
    }
} /* namespace snake */
