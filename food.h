#ifndef FOOD_H_
#define FOOD_H_

#include "point.h"
#include "sprite_sheet.h"

namespace snake
{
    class Food
    {
    public:
        Food(snake::Point position,
                std::unique_ptr<snake::SpriteSheet> ss);

        const snake::Point& getPosition() const noexcept;

        SpriteSheet& getSpriteSheet() const;

    protected:
        snake::Point position;
        std::unique_ptr<SpriteSheet> ss;
    };
} /* namespace snake */

#endif /* FOOD_H_ */
